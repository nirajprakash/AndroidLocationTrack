package io.thedroid.locationgrab

import android.app.Application

class ApplicationLocation: Application() {

    companion object {
        lateinit var instance: ApplicationLocation

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}