package io.thedroid.locationgrab

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import io.thedroid.locationgrab.local.LocalPref
import android.widget.Toast
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnFailureListener
import android.app.Activity
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import io.thedroid.locationgrab.service.ServiceTimer
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import java.lang.Exception
import java.util.*


class MainActivity : AppCompatActivity(), AnkoLogger {

    //https://www.androidhive.info/2015/02/android-location-api-using-google-play-services/
    private val REQUEST_CHECK_SETTINGS = 100
    private lateinit var dialogPermissionListener: CompositePermissionListener


    var permissionListener: PermissionListener = object : PermissionListener {
        override fun onPermissionGranted(response: PermissionGrantedResponse?) {

            var calendar = Calendar.getInstance()
            var time: Long = calendar.timeInMillis / 1000
            var trackerId = LocalPref.getTrackerId(this@MainActivity)
            info { trackerId }
            if (TextUtils.isEmpty(trackerId)) {
                LocalPref.setTrackerId(this@MainActivity, time.toString())
                trackerId = time.toString();
            }
            tracker_id.text = trackerId
            startLocationUpdates()

        }


        override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken) {
            info("Requesting location updates")
            token.continuePermissionRequest();
        }

        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
            info("Deny location updates")
        }
    }

    var mIsServiceRunning = false
    lateinit var mLocationSettingRequest: LocationSettingsRequest
    lateinit var mSettingsClient: SettingsClient
    lateinit var mLocationRequest: LocationRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        info { "sasa" }
        verbose { "5453trf" }
        mIsServiceRunning = LocalPref.getIsRunning(this)
        info { "isServerRunning: " + mIsServiceRunning }

        if(mIsServiceRunning){
            btn_service.text = "STOP SERVICE"
            tracker_id.text = LocalPref.getTrackerId(this)
        }

        information.text = LocalPref.getLastTime(this)

        try {
                var pInfo = this.packageManager.getPackageInfo(packageName, 0);
                var version = pInfo.versionName
                if (!TextUtils.isEmpty(version)) {
                    versionText.text = version
                }
        } catch (e: PackageManager.NameNotFoundException ) {
                e.printStackTrace();
            }
//        var id = Local


        initLocationrequestBuilder()

        btn_service.setOnClickListener { onClickBtn(it) }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Toast.makeText(applicationContext, "Started 2! ", Toast.LENGTH_SHORT).show()

        info("cane ")
        if (requestCode == REQUEST_CHECK_SETTINGS) {

            info("cane2 ")

            info("cane21 " + resultCode)
            when (resultCode) {
                Activity.RESULT_OK -> {

                    info("cane221 " + resultCode)
                    info("User agreed to make required location settings changes.")

                    onGpsEnabled()

                }
                Activity.RESULT_CANCELED -> {

                    info("cane321 " + resultCode)
                    info("User chose not to make required location settings changes.")
                    //mRequestingLocationUpdates = false;
                }
            }
        }
    }


    /* ************************************************************************
     *                              Permission
     */


    fun createPermissionListener() {

        val dialogOnDeniedPermissionListener = DialogOnDeniedPermissionListener.Builder.withContext(this)
                .withTitle("Permission Denied")
                .withMessage("Permission Needed for location")
                .withButtonText(android.R.string.ok)
                .withIcon(R.mipmap.ic_launcher_round)
                .build()
        dialogPermissionListener = CompositePermissionListener(permissionListener,
                dialogOnDeniedPermissionListener)
    }

    private fun onClickBtn(it: View) {

        info { }
        if (mIsServiceRunning) {
            LocalPref.setIsRunning(this@MainActivity, false)
            AlarmReceiver.cancelAlarm(this@MainActivity)
            mIsServiceRunning = false
            btn_service.text = "START SERVICE"

            info { "clicked 1" }

        } else {

            createPermissionListener()
            Dexter.withActivity(this@MainActivity)
                    .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(dialogPermissionListener)
                    .check();

            info { "clicked 2" }

        }


    }

    /* *************************************************************
     *                               Location
     */


    fun initLocationrequestBuilder() {

        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 20 * 1000
        mLocationRequest.fastestInterval = 5 * 1000


        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)

        builder.setAlwaysShow(true)
        mLocationSettingRequest = builder.build()
    }


    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    fun startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingRequest)
                .addOnSuccessListener(this, object : OnSuccessListener<LocationSettingsResponse> {
                    override fun onSuccess(locationSettingsResponse: LocationSettingsResponse?) {
                        info { locationSettingsResponse }

                        onGpsEnabled()
//                        Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show();
                    }

                })
                .addOnFailureListener(object : OnFailureListener {
                    override fun onFailure(exception: Exception) {

                        var ex = exception as ApiException
                        when (ex.statusCode) {
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {

//                                Toast.makeText(applicationContext, "Started location updates 2! ", Toast.LENGTH_SHORT).show()

                                var rae = exception as ResolvableApiException
                                rae.startResolutionForResult(this@MainActivity, REQUEST_CHECK_SETTINGS)

                            }


                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                                var errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings."

//                                Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                })

    }


    private fun onGpsEnabled() {
        startServiceLocation()
        LocalPref.setIsRunning(this, true)
        mIsServiceRunning = true
        btn_service.text = "Stop Service"

    }

    /* ***************************************************************
     *                          Service
     */

    // Monitors the state of the connection to the service.


    fun startServiceLocation() {


        info { "starting service" }
//        val intent = Intent()
//        intent.putExtra("val", "testing service")
//        ServiceTimer.enqueueWork(this, intent)
        AlarmReceiver.setAlarm(true, ApplicationLocation.instance.applicationContext)

        contentView?.let { Snackbar.make(it, "Service Started", 4000) }

    }


}
