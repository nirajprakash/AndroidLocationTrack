package io.thedroid.locationgrab

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.app.PendingIntent
import android.app.AlarmManager
import android.util.Log
import io.thedroid.locationgrab.service.ServiceTimer
import org.jetbrains.anko.AnkoLogger
import android.support.v4.app.AlarmManagerCompat.setExactAndAllowWhileIdle
import android.os.Build
import android.support.v4.app.AlarmManagerCompat.setExact
import io.thedroid.locationgrab.local.LocalPref
import org.jetbrains.anko.info
//https://github.com/OpenLocate/openlocate-android/blob/90f62ae6b2c359818cde781d796b1e5086a4b9b6/openlocate/src/main/java/com/openlocate/android/core/LocationServiceHelper.java#L347

class AlarmReceiver : BroadcastReceiver(), AnkoLogger {


    companion object {

        val CUSTOM_INTENT = "com.test.intent.action.ALARM"
        fun cancelAlarm(ctx: Context) {

            val alarm = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            Log.i("AlarmReceiver: ", "canecl")

            /* cancel any pending alarm */
            alarm.cancel(getPendingIntent())
        }

        fun setAlarm(force: Boolean, ctx: Context) {
            cancelAlarm(ctx)
            val alarm = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            // EVERY X MINUTES
            val delay = (1000 * 60 * 12).toLong()
            var `when` = System.currentTimeMillis()
            if (!force) {
                `when` += delay
            }

            Log.i("AlarmReceiver", "setting alarm");
            /* fire the broadcast */
            setAlarm(alarm, `when`, getPendingIntent())
//            alarm.set(AlarmManager.RTC_WAKEUP, `when`, getPendingIntent())
        }

        private fun getPendingIntent(): PendingIntent {

            var ctx: Context = ApplicationLocation.instance.applicationContext  /* get the application context */
            var alarmIntent = Intent(ctx, AlarmReceiver::class.java)
            alarmIntent.action = CUSTOM_INTENT

            return PendingIntent.getBroadcast(ctx, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
        }

        private fun setAlarm(alarmManager: AlarmManager, `when`: Long, pendingIntent: PendingIntent) {
            val SDK_INT = Build.VERSION.SDK_INT
            if (SDK_INT < Build.VERSION_CODES.KITKAT)
                alarmManager.set(AlarmManager.RTC_WAKEUP, `when`, pendingIntent)
            else if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M)
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, `when`, pendingIntent)
            else if (SDK_INT >= Build.VERSION_CODES.M) {
                LocalPref.setLastTime(ApplicationLocation.instance.applicationContext, "alarm set")

                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, `when`, pendingIntent)
            }
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        /* enqueue the job */
        info { "enque work" }
        LocalPref.setLastTime(context, "start job")
        ServiceTimer.enqueueWork(context, intent)
    }


}