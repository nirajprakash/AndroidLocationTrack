package io.thedroid.locationgrab.service

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.*
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat


import org.jetbrains.anko.*
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest
import io.thedroid.locationgrab.Utils
import android.widget.Toast
import io.reactivex.Observer
import okhttp3.ResponseBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.thedroid.locationgrab.api.ModelGps
import io.thedroid.locationgrab.api.PostMan
import io.thedroid.locationgrab.local.LocalPref
import java.io.IOException
import java.util.*

//https://www.androidhive.info/2015/02/android-location-api-using-google-play-services/
//https://www.journaldev.com/13347/android-location-google-play-services
//http://devdeeds.com/android-location-tracking-in-background-service/
//important
//https://stackoverflow.com/questions/48265730/in-oreo-8-0-0-api-26-how-to-get-a-location-services-update-when-the-app-i
class ServiceLocation : Service(), AnkoLogger {


    private val TAG = ServiceLocation::class.java.simpleName

    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    companion object {
        val STOP_SERVICE: String = "serviceLocation.stop"

        private val FOREGROUND_SERVICE_TAG = 1001
    }

    private val mBinder = ServiceLocationBinder()

    private lateinit var mFusedLocationClient: FusedLocationProviderClient


    private lateinit var mLocationCallback: LocationCallback
    private lateinit var mLocationRequest: LocationRequest


    private lateinit var mLocation: Location

    private lateinit var mServiceHandler: Handler

    override fun onCreate() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground(0, Notification())
//        }
//        super.onCreate()
        info { "Service OnCreate" }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult!!.lastLocation)
            }
        }
        createLocationRequest()

        //getLastLocation();

        requestLocationUpdates()

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
//        mServiceHandler = Handler(handlerThread.looper)

        info { "satart: " + "sdsd" }
//
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        info { "Service Started command" }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground(0, Notification())

            LocalPref.setServiceTime(applicationContext, "Start Notification")
            val notification = NotificationCompat.Builder(this)
                    .build()
            startForeground(FOREGROUND_SERVICE_TAG, notification)

        }
        val stopService = intent.getBooleanExtra(STOP_SERVICE,
                false)

        // We got here because the user decided to remove location updates from the notification.
        if (stopService) {
            removeLocationUpdates()
            stopSelf()
        }
       // Tells the system to not try to recreate the service after it has been killed.
        return Service.START_NOT_STICKY
    }

    /**
     * Sets the location request parameters.
     */
    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }


    private fun getLastLocation() {
        try {
            mFusedLocationClient.lastLocation
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful && task.result != null) {
                            mLocation = task.result
                        } else {
                            warn { "Failed to get location." }
                        }
                    }
        } catch (unlikely: SecurityException) {
            error { "Lost location permission.$unlikely" }
        }

    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun requestLocationUpdates() {
        info("Requesting location updates")
        Utils.setRequestingLocationUpdates(this, true)
//        startService(Intent(applicationContext, ServiceLocation::class.java))
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper())
        } catch (unlikely: SecurityException) {
            Utils.setRequestingLocationUpdates(this, false)
            error("Lost location permission. Could not request updates. $unlikely")
        }

    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    private fun removeLocationUpdates() {
        info { "Removing location updates" }
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback)
            Utils.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            Utils.setRequestingLocationUpdates(this, true)
            error { "Lost location permission. Could not remove updates. $unlikely" }
        }

    }

    private fun onNewLocation(location: Location) {
        info("new location" + location)
        val trackerId= LocalPref.getTrackerId(baseContext)
        mLocation = location

        val dataGps = ModelGps(trackerId, mLocation.latitude, mLocation.longitude,
                Calendar.getInstance().timeInMillis)
        postLocationData(dataGps)
    }


    private fun postLocationData(dataGps: ModelGps) {
        LocalPref.setLastTime(applicationContext, "PostLocation Data: ${dataGps.toString()}")

        PostMan.create().postLocation(dataGps).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseBody> {

                    override fun onNext(responseBody: ResponseBody) {

                        try {
                            val response = responseBody.string()
                            info { response }

//                                Toast.makeText(this@ServiceLocation, response, Toast.LENGTH_SHORT).show()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        info { "service send" }

                    }

                    override fun onComplete() {

                        info { "service send 2" }
                        this@ServiceLocation.endLocationUpdate()
                    }

                    override fun onSubscribe(d: Disposable) {

                        info { "service send 3" }
                    }

                    override fun onError(e: Throwable) {

                        info { "service send 4" }
                    }



                });
    }


    private fun endLocationUpdate() {
        removeLocationUpdates()
        stopSelf()
    }

    /* ************************************************************
     *                          Binder
     */

    override fun onBind(p0: Intent?): IBinder {
        stopForeground(true);
//        mChangingConfiguration = false;
        return mBinder;
    }

    inner class ServiceLocationBinder : Binder() {
        fun getService(): ServiceLocation {
            return this@ServiceLocation
        }
    }

}