package io.thedroid.locationgrab.service

import android.app.Notification
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.app.JobIntentService
import android.support.v4.content.ContextCompat
import io.thedroid.locationgrab.AlarmReceiver
import io.thedroid.locationgrab.ApplicationLocation
import io.thedroid.locationgrab.local.LocalPref
import org.jetbrains.anko.*

//https://materialdoc.com/components/buttons/

//https://medium.com/@benexus/background-services-in-android-o-862121d96c95

class ServiceTimer : JobIntentService(), AnkoLogger {
    /* Give the Job a Unique Id */

    companion object {
        private const val JOB_ID = 123

        fun enqueueWork(ctx: Context, intent: Intent) {
            enqueueWork(ctx, ServiceTimer::class.java, JOB_ID, intent)
        }
    }


    override fun onHandleWork(intent: Intent) {
        /* your code here */
        /* reset the alarm */
        info { "handle work" }
        LocalPref.setLastTime(applicationContext, "HandleIntent")
        startIntentService()

        info { "currentPosition: " + "woe" }
        AlarmReceiver.setAlarm(false, ApplicationLocation.instance.applicationContext)

        try {
            Thread.sleep(40 * 1000)

            stopSelf()
            // 1000 is one second, ten seconds would be 10000
        } catch (e: InterruptedException) {

            stopSelf()
        }

    }

    private fun startIntentService() {
        // Create an intent for passing to the intent service responsible for fetching the address.
        var intent = Intent(ApplicationLocation.instance.applicationContext, ServiceLocation::class.java)

        // Pass the result receiver as an extra to the service.
//        intent.putExtra(Constants.RECEIVER, mResultReceiver)

        // Pass the location data as an extra to the service.
//        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation)

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.

        info { "satart: " + "sdsd" }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(this,intent)
            //startForeground(0, Notification())
        } else
            startService(intent)
    }




}
