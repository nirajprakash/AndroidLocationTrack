package io.thedroid.locationgrab.api

data class ModelGps(
    val trackerId: String,
    val lat: Double,
    val lng: Double,
    val date: Long){
}