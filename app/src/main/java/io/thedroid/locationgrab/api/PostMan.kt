package io.thedroid.locationgrab.api


import io.reactivex.Observable
import okhttp3.ResponseBody;
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


class PostMan {

    companion object {
        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("https://proconnect.intutrack.com:9000/api/")
                    .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }


    interface ApiInterface {


        @GET
        fun fetchInterval(@Url url: String): Observable<ResponseBody>

//        @FormUrlEncoded

        @Headers("Content-Type: application/json")
        @POST("appData")
        fun postLocation(@Body params: ModelGps): Observable<ResponseBody>




    }
}