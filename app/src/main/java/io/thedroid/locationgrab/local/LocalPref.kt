package io.thedroid.locationgrab.local

import android.content.Context
import android.content.SharedPreferences
import android.provider.CalendarContract
import java.util.*

class LocalPref {

    companion object {
        val STORAGEKEY="locationgrab.pref"
        private val TRACKER_ID = "trackerId"
        private val TIMER = "timer"
        private val IS_SERVICE_RUNNING ="isServiceRunning"
        fun getSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(STORAGEKEY, 0)
        }


        fun setTrackerId(context: Context, trackerId: String): Boolean {
            val sp = getSharedPreferences(context)
            return setTrackerId(sp, trackerId)
        }

        fun setIsRunning(context: Context, value: Boolean): Boolean {
            val sp = getSharedPreferences(context)
            var editor = sp.edit()
            editor.putBoolean(IS_SERVICE_RUNNING, value)
                    .apply()
            return true
        }

        fun getIsRunning(context: Context): Boolean {
            val sp = getSharedPreferences(context)
            return sp.getBoolean(IS_SERVICE_RUNNING, false)

        }

        private fun setTrackerId(sp: SharedPreferences, id: String): Boolean {
            var editor = sp.edit()
            editor.putString(TRACKER_ID, id)
                    .apply()

            return true
        }

        fun getTrackerId(context: Context): String {

            var sp: SharedPreferences = getSharedPreferences(context)
            return sp.getString(TRACKER_ID, "")

        }

        fun setLastTime(context: Context, message: String): Boolean {
            var date = Calendar.getInstance().time.toString()
            date += " $message"

            var sp: SharedPreferences = getSharedPreferences(context)
            var editor = sp.edit()
            editor.putString(TIMER, date)
                    .apply()

            return true
        }

        fun getLastTime(context: Context): String {

            var sp: SharedPreferences = getSharedPreferences(context)
            return sp.getString(TIMER, "")

        }

        fun setServiceTime(applicationContext: Context?, s: String) {
//            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

}